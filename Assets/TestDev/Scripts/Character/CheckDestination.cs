using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDestination : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Destination"))
        {
            transform.parent.gameObject.SetActive(false);
        }
    }
}
