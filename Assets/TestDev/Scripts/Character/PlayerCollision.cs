using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class PlayerCollision : MonoBehaviour
{
    private const string COIN_TAG = "Coin";
    private const string OBSTACLE_TAG = "Obstacle";
    private const string DESTINATION_TAG = "Destination";
    private const string ITEMSPEED_TAG = "ItemSpeed";
    [SerializeField] private ParticleSystem collectCoinVFX;
    [SerializeField] private ParticleSystem takeItemVFX;
    [SerializeField] private TextMeshProUGUI coinTxt;
    private PlayerMovement playerMovement;
    public GameManager gameManager;
    public AudioSource audioSource;
    public AudioClip hitSfx;
    public AudioClip coinSfx;
    public float countDownChangeSpeed;

    private void Awake()
    {
        playerMovement= GetComponentInParent<PlayerMovement>();
        countDownChangeSpeed = 0;
    }

    private void Update()
    {
        if(countDownChangeSpeed >0)
        {
            countDownChangeSpeed -= Time.deltaTime;
            if(countDownChangeSpeed <= 0)
            {
                playerMovement.SetSpeedToStartSpeed();
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag(COIN_TAG))
        {
            PlayerData.SetCoin(10);
            coinTxt.text = PlayerData.GetCoinn().ToString();
            collectCoinVFX.Play();
            Destroy(collision.gameObject);
            audioSource.PlayOneShot(coinSfx);
        }
        if(collision.gameObject.CompareTag(OBSTACLE_TAG))
        {
            audioSource.PlayOneShot(hitSfx);
            playerMovement.SetSpeed(true);
            countDownChangeSpeed = 2;

            //StartCoroutine(IEDelaySetCurrentSpeedToStartSpeed());
        }
        if (collision.gameObject.CompareTag(DESTINATION_TAG))
        {
            gameManager.EndGame(true);
        }
        if (collision.gameObject.CompareTag(ITEMSPEED_TAG))
        {
            takeItemVFX.Play();
            audioSource.PlayOneShot(coinSfx);
            playerMovement.SetSpeed(false);
            countDownChangeSpeed = 1;
            Destroy(collision.gameObject);
          //  StartCoroutine(IEDelaySetCurrentSpeedToStartSpeed());
        }
    }
}
