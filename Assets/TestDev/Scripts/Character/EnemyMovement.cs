using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PathCreation.Examples;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private Transform enemyTransform;
    [SerializeField] private float limitValue;

    private float countDownMoveEnemy;
    private float timeRandom;

    private void Start()
    {
        RandomTime();
        countDownMoveEnemy = timeRandom;
    }

    private void Update()
    {
        countDownMoveEnemy += Time.deltaTime;
        if(countDownMoveEnemy > timeRandom)
        {
            MoveEnemy();
            countDownMoveEnemy = 0;
            RandomTime();
        }
    }

    private void RandomTime()
    {
        timeRandom = Random.Range(0.5f,2f);
    }

    private void MoveEnemy()
    {
        float halfScreen = Screen.width / 2;
        var randomInputX = Random.Range(10, Screen.width - 5 / 2);
        float xPos = (randomInputX - halfScreen) / halfScreen;
        float finalXPos = Mathf.Clamp(xPos * limitValue, -limitValue, limitValue);
        enemyTransform.DOLocalMove(new Vector3(finalXPos, 0, 0), 1).SetEase(Ease.Linear);
    }
}
