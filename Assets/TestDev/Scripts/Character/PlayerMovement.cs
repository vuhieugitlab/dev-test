using PathCreation.Examples;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float limitValue;
    private PathFollower pathFollower;
    private float startSpeed;
    private float currentSpeed;
    private void Awake()
    {
        pathFollower = GetComponent<PathFollower>();
       
    }
    private void Start()
    {
        startSpeed = 8;
        currentSpeed = startSpeed;
        pathFollower.speed = currentSpeed;
    }

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            MovePlayer();
        }
    }

    private void MovePlayer()
    {
        float halfScreen = Screen.width/2;
        float xPos  = (Input.mousePosition.x - halfScreen)/ halfScreen;
        float finalXPos = Mathf.Clamp(xPos * limitValue,-limitValue,limitValue);
        playerTransform.localPosition = new Vector3(finalXPos, 0, 0);
    }

    public void SetSpeed(bool isObstacle)
    {
        
        if(isObstacle && currentSpeed >= startSpeed)
        {
            currentSpeed /= 2;
            pathFollower.speed = currentSpeed;
        }
        if(!isObstacle && currentSpeed <= startSpeed)
        {
            currentSpeed *= 1.5f;
            pathFollower.speed = currentSpeed;
        }
    
    }

    public void SetSpeedToStartSpeed()
    {
        currentSpeed = startSpeed;
        pathFollower.speed = currentSpeed;
    }
}
