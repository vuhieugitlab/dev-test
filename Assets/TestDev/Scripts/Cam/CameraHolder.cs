using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHolder : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    private Vector3 initialRotation;

    private void Start()
    {
        initialRotation = transform.eulerAngles;

    }
    private void Update()
    {
        transform.position = playerTransform.position;
        transform.eulerAngles = new Vector3(playerTransform.eulerAngles.x, playerTransform.eulerAngles.y, 0);
    }
}
