using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerData
{
    public static AllData allData;
    static PlayerData()
    {
        allData = JsonUtility.FromJson<AllData>(PlayerPrefs.GetString("AllData"));
        if (allData == null)
        {
            
            allData = new AllData
            {
                coin = 0
            };
            SaveData();
        }
    }
    public static void SaveData()
    {
        var data = JsonUtility.ToJson(allData);
        PlayerPrefs.SetString("AllData", data);
    }

    public static int GetCoinn()
    {
       return allData.GetCoin();
    }

    public static void SetCoin(int coinValue)
    {
        allData.SetCoin(coinValue);
        SaveData();
    }

}

public class AllData
{
    public int coin;

    public int GetCoin()
    {
        return coin;
    }
    public void SetCoin(int value)
    {
        coin += value;
    }
}
