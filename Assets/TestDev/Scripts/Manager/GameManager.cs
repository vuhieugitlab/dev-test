using PathCreation;
using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public PathFollower playerPathFollower;
    public PathFollower enemyPathFollower;
    public PlayerMovement playerMovement;
    public GameObject replayButton;
    public EnemyMovement enemyMovement;

    public Button playButton;
    public List<GameObject> mapList = new List<GameObject>();
    public PathFollower playerFollowPath;
    public PathFollower enemyFollowPath;
    public static bool isReplay = false;

    public TextMeshProUGUI resulttxt;
    public void Start()
    {
        if (isReplay)
            StartGame();
    }

    public void StartGame()
    {
        foreach (var item in mapList)
        {
            item.SetActive(false);
        }
        int radomMap = UnityEngine.Random.Range(0, mapList.Count);
        mapList[radomMap].SetActive(true);
        var pathCreator = mapList[radomMap].GetComponentInChildren<PathCreator>();
        if (pathCreator == null) return;
        playerFollowPath.pathCreator = pathCreator;
        playerMovement.enabled = true;
        playButton.gameObject.SetActive(false);
        replayButton.gameObject.SetActive(false);
        StartCoroutine(IEDelaySetActiveEnemy(pathCreator));
    }


    public void EndGame(bool win)
    {
        enemyPathFollower.pathCreator = null;
        playerPathFollower.pathCreator = null;
        playerMovement.enabled = false;
        StartCoroutine(IEDelayEnableRePlayButton());
        enemyMovement.enabled = false;
        isReplay = true;
        resulttxt.gameObject.SetActive(true);
        if (win)
        {
            playerMovement.GetComponentInChildren<Animator>().SetBool("Win", true);
            resulttxt.text = "WIN";
        }
        else
        {
            enemyMovement.GetComponentInChildren<Animator>().SetBool("Win", true);
            resulttxt.text = "LOSE";
        }
    }

    IEnumerator IEDelaySetActiveEnemy(PathCreator path)
    {
        yield return new WaitForSeconds(1f);
        playerFollowPath.gameObject.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        
        enemyFollowPath.pathCreator = path;
        enemyMovement.enabled = true;
        enemyFollowPath.gameObject.SetActive(true);
        
    }


    IEnumerator IEDelayEnableRePlayButton()
    {
        yield return new WaitForSeconds(2);
        replayButton.SetActive(true);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
