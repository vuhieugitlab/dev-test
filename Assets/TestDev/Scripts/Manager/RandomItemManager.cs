using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomItemManager : MonoBehaviour
{
    public List<GameObject> itemRandomList = new List<GameObject>();

    private float coolDown;
    private float timeRandomSpawnItem;

    private void Start()
    {
        coolDown = 0;
        RandomTimeSpawnItem();
    }

    void Update()
    {
        coolDown += Time.deltaTime;
        if (coolDown > timeRandomSpawnItem)
        {
            SpawnItem();
            coolDown = 0;
            RandomTimeSpawnItem();
        }

    }

    public void SpawnItem()
    {
        float rate = Random.value;
        if(rate >= 0 && rate <0.5f)
        {
            //spawnCoin
            var item = Instantiate(itemRandomList[0], transform.position, transform.rotation);
            var itemCurrentPos = item.transform.position;
            item.transform.position = new Vector3(itemCurrentPos.x, 1, itemCurrentPos.z);
        }
        else if(rate <= 0.9)
        {
            var item = Instantiate(itemRandomList[1], transform.position, transform.rotation);
          
        }
        else
        {
            var item = Instantiate(itemRandomList[2], transform.position, transform.rotation);
            var itemCurrentPos = item.transform.position;
            item.transform.position = new Vector3(itemCurrentPos.x, 1, itemCurrentPos.z);
        }
     //   int itemIndex = Random.Range(0, itemRandomList.Count);
      
    }

    private void RandomTimeSpawnItem()
    {
        timeRandomSpawnItem = Random.Range(0.5f, 2);

    }
}
