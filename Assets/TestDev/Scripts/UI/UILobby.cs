using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using PathCreation;
using PathCreation.Examples;

public class UILobby : MonoBehaviour
{
    public Button playButton;
    public Button replayButton;
    public GameManager gameManager;
    private void Awake()
    {
        playButton.onClick.AddListener(gameManager.StartGame);
        replayButton.onClick.AddListener(gameManager.ReloadScene);
    }

   

    
}
