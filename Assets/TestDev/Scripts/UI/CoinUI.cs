using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinUI : MonoBehaviour
{
    public TextMeshProUGUI coinTxt;

    private void Start()
    {
        UpdateCoin();
    }

    public  void UpdateCoin()
    {
        coinTxt.text = PlayerData.GetCoinn().ToString();
    }
}
